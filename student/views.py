from django.contrib import messages
from django.contrib.auth import authenticate, login, update_session_auth_hash, logout
from django.contrib.auth.forms import PasswordChangeForm
from django.http import JsonResponse
from django.shortcuts import render, redirect

from staff.models import StaffProfile
from student.models import Unit, StudentProfile, Department
from student.forms import LoginForm, StudentProfileForm
from missmark.models import StudentMissingMark
from missmark.forms import RegisterResitForm


# Create your views here.
def login_page(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            request.session['email'] = email
            cd = form.cleaned_data
            user = authenticate(email=cd['email'],
                                password=cd['password'])
            if user is not None:
                login(request, user)
                if request.user.student:
                    return redirect('index')
                else:
                    messages.error(request, 'Sorry you are not a student')
                    return render(request, 'accounts/login.html', {'form': form})
            else:
                messages.error(request, 'Sorry you are not a student')
                form = LoginForm()
                context = {'form': form}
                return render(request, 'accounts/login.html', context)
        else:
            messages.error(request, 'Sorry you are not a student')
            form = LoginForm()
            context = {'form': form}
            return render(request, 'accounts/login.html', context)

    else:
        form = LoginForm()
    context = {'form': form}
    return render(request, 'accounts/login.html', context)


def register(request):
    if request.method == 'POST':
        form = StudentProfileForm(request.POST)

        if form.is_valid():
            form.save()
            email = form.cleaned_data['email']
            password = form.cleaned_data['password1']
            user = authenticate(email=email, password=password)
            login(request, user)
            return redirect('index')
    else:
        form = StudentProfileForm()
    context = {'form': form}
    return render(request, 'accounts/register.html', context)


def index(request):
    if request.session.session_key is None:
        return redirect("login")
    else:
        if request.method == 'POST':
            form = RegisterResitForm(request.POST)
            if form.is_valid():
                resit = form.save(commit=False)
                re = StudentProfile.objects.get(
                    student_no=request.POST.get('student_no', request.user.studentprofile.student_no))
                print(re)
                resit.student_no = re
                resit.seen_status = request.POST['seen_status']
                resit.mm_status = request.POST['mm_status']
                resit.save()
                return redirect('current')
        else:
            form = RegisterResitForm(request.POST)
            cont = {'form': form}
            return render(request, 'app/index.html', cont)


def change_password(request):
    if request.session.session_key is None:
        return redirect("login")
    else:
        if request.method == 'POST':
            form = PasswordChangeForm(request.user, request.POST)
            if form.is_valid():
                user = form.save()
                update_session_auth_hash(request, user)  # Important!
                messages.success(request, 'Your password was successfully updated!')
                return redirect('index')
            else:
                messages.error(request, 'Please correct the error below.')
        else:
            form = PasswordChangeForm(request.user)
        return render(request, 'accounts/change_password.html', {'form': form})


def logout_view(request):
    logout(request)
    return redirect('login')


def current(request):
    if request.session.session_key is None:
        return redirect("login")
    else:
        resit_list = StudentMissingMark.objects.filter(mm_status=False, seen_status=False,
                                                       student_no=request.user.studentprofile.student_no)
        print(request.user.studentprofile.student_no)
        context = {'resit_list': resit_list}
        return render(request, 'app/current.html', context)


def pending(request):
    if request.session.session_key is None:
        return redirect("login")
    else:
        resit_list = StudentMissingMark.objects.filter(mm_status=False, seen_status=True,
                                                       student_no=request.user.studentprofile.student_no)
        context = {'resit_list': resit_list}
        return render(request, 'app/pending.html', context)


def previous(request):
    if request.session.session_key is None:
        return redirect("login")
    else:
        resit_list = StudentMissingMark.objects.filter(mm_status=True,
                                                       student_no=request.user.studentprofile.student_no)
        context = {'resit_list': resit_list}
        return render(request, 'app/Previous.html', context)


def load_departments(request):
    faculty_id = request.GET.get('faculty')
    departments = Department.objects.filter(faculty_id=faculty_id).order_by('id')
    return render(request, 'app/departments_dropdown_list_options.html', {'departments': departments})


def load_lecs(request):
    department_id = request.GET.get('department')
    lecs = StaffProfile.objects.filter(department_id=department_id).order_by('staff_no')
    return render(request, 'app/taught_by_dropdown_list_options.html', {'lecs': lecs})


def load_units(request):
    department_id = request.GET.get('department')
    user_id = int(request.user.studentprofile.department_id)
    dep = int(request.GET.get('department'))
    print(user_id, dep)
    if user_id == dep:
        print(1)
        units = Unit.objects.filter(department_id=department_id, unit_type=1).order_by('year_offered')
        return render(request, 'app/units_dropdown_list_options.html', {'units': units})
    elif user_id != dep:
        print(2)
        units = Unit.objects.filter(department_id=department_id, unit_type=2).order_by('year_offered')
        return render(request, 'app/units_dropdown_list_options.html', {'units': units})


def validate_missing_marks(request):
    unit = request.GET.get('unit', None)
    data = {
        'is_taken': StudentMissingMark.objects.filter(unit=unit,
                                                      student_no=request.user.studentprofile.student_no).exists()
    }
    if data['is_taken']:
        data['error_message'] = 'You have already booked this unit'
    return JsonResponse(data)
