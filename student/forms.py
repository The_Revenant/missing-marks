from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from student import models
from student.models import Faculty, Department
from django.core.validators import RegexValidator

email_regex = "(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"
# student_no_regex = "(^[A-Z]{1,2}[0-9]{2}+\\/[0-9]{5}+\\/[0-9]{2}+$)"


class RegisterForm(forms.ModelForm):
    email = forms.EmailField(widget=forms.EmailInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter Your Email'}))
    fullname = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter Your Full Name'}))
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter Your New Password'}))
    password2 = forms.CharField(label='Password Confirmation', widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'placeholder': 'Confirm The Password'}))

    class Meta:
        model = models.User
        fields = ('email', 'fullname')

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords do not match")
        return password2

    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        user.set_password(self.cleaned_data["password1"])
        user.active = True
        user.student = True
        if commit:
            user.save()
        return user


class StudentProfileForm(forms.ModelForm):
    email = forms.EmailField(validators=[RegexValidator(email_regex)], widget=forms.EmailInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter Your Email', 'name': 'email', 'type': 'email'}))
    fullname = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter Your Full Name'}))
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter Your New Password'}))
    password2 = forms.CharField(label='Password Confirmation', widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'placeholder': 'Confirm The Password'}))
    student_no = forms.CharField(max_length=13, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter Your Registration Number'}))
    phone_no = forms.CharField(max_length=10, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter Your Phone Number'}))
    faculty = forms.ModelChoiceField(queryset=Faculty.objects.all(),
                                     widget=forms.Select(attrs={'class': 'form-control'}))
    department = forms.ModelChoiceField(queryset=Department.objects.all().exclude(name__exact="Other"),
                                        widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = models.StudentProfile
        widgets = {'gender': forms.Select(attrs={'class': 'form-control'}),
                   'department': forms.Select(attrs={'class': 'form-control'}),
                   }
        fields = (
            'email', 'fullname', 'student_no', 'phone_no', 'gender', 'password1', 'password2', 'faculty', 'department',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["department"].queryset = Department.objects.none()

        if 'faculty' in self.data:
            try:
                faculty_id = int(self.data.get('faculty'))
                self.fields['department'].queryset = Department.objects.filter(faculty_id=faculty_id).order_by('name')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['department'].queryset = self.instance.faculty.department_set.order_by('name')

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords do not match")
        return password2

    def save(self, commit=True):
        user = super(StudentProfileForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        user.set_password(self.cleaned_data["password1"])
        user.active = True
        user.student = True
        if commit:
            user.save()
        return user


class LoginForm(forms.Form):
    email = forms.EmailField(label='Email', widget=forms.TextInput(attrs={'class': 'form-control',
                                                                          'placeholder': 'Enter Your Email Address'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control',
                                                                 'placeholder': 'Enter Your Password'}))


class IndexForm(forms.Form):
    unit_code = forms.CharField(max_length=20, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter Unit Code', 'name': 'q'}))


class UserAdminCreationForm(forms.ModelForm):
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password Confirmation', widget=forms.PasswordInput)

    class Meta:
        model = models.User
        fields = ('fullname', 'email',)

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords do not match")
        return password2

    def save(self, commit=True):
        user = super(UserAdminCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserAdminChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = models.User
        fields = ('fullname', 'email', 'password', 'active', 'admin')

    def clean_password(self):
        return self.initial["password"]
