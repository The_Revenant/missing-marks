from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager
)
from MissingMarks import settings


# Create your models here.


class UserManager(BaseUserManager):
    def create_user(self, email, fullname, password=None, is_student=False, is_active=True, is_staff=False,
                    is_admin=False):
        if not email:
            raise ValueError("User must have an email address")
        if not password:
            raise ValueError("User must have a password")
        if not fullname:
            raise ValueError("User Must Have a Name")
        user_obj = self.model(
            email=self.normalize_email(email),
            fullname=fullname
        )
        user_obj.set_password(password)  # change password
        user_obj.student = is_student
        user_obj.staff = is_staff
        user_obj.active = is_active
        user_obj.admin = is_admin
        user_obj.save(using=self._db)
        return user_obj

    def create_studentuser(self, email, fullname, password=None):
        user = self.create_user(email, fullname, password=password, is_student=True)
        return user

    def create_staffuser(self, email, fullname, password=None):
        user = self.create_user(email, fullname, password=password, is_staff=True)
        return user

    def create_superuser(self, email, fullname, password=None):
        user = self.create_user(email, fullname, password=password, is_staff=True, is_admin=True)
        return user


class User(AbstractBaseUser):
    email = models.EmailField(max_length=30, unique=True, primary_key=True)
    fullname = models.CharField(max_length=255, blank=False, null=False)
    active = models.BooleanField(default=True)  # can login
    student = models.BooleanField(default=False)  # Student User
    staff = models.BooleanField(default=False)
    admin = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True)

    USERNAME_FIELD = 'email'  # Username

    REQUIRED_FIELDS = ['fullname']

    objects = UserManager()

    def __str__(self):
        return self.email

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    @staticmethod
    def has_perm(perm, obj=None):
        return True

    @staticmethod
    def has_module_perms(app_label):
        return True

    @property
    def is_student(self):
        return self.student

    @property
    def is_active(self):
        return self.active

    @property
    def is_admin(self):
        return self.admin

    @property
    def is_staff(self):
        return self.staff


class StudentProfile(User):
    student_no = models.CharField(max_length=15, primary_key=True)
    phone_no = models.CharField(max_length=10, unique=True)
    faculty = models.ForeignKey('Faculty', on_delete=models.CASCADE, default=1)
    department = models.ForeignKey('Department', on_delete=models.CASCADE, default=1)
    gender_choices = ((2, 'MALE'), (3, 'FEMALE'))
    gender = models.PositiveSmallIntegerField(choices=gender_choices)

    def __str__(self):
        return self.student_no


class Faculty(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        verbose_name = 'Faculty'
        verbose_name_plural = 'Faculties'

    def __str__(self):
        return self.name


class Department(models.Model):
    faculty = models.ForeignKey('Faculty', on_delete=models.CASCADE)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Unit(models.Model):
    department = models.ForeignKey('Department', on_delete=models.CASCADE)
    year_choices = ((1, 1), (2, 2), (3, 3), (4, 4))
    year_offered = models.PositiveSmallIntegerField(choices=year_choices, default=1)
    unit_choices = ((1, 'CORE'), (2, 'SERVICE'))
    unit_type = models.PositiveSmallIntegerField(choices=unit_choices)
    unit_code = models.CharField(max_length=20, primary_key=True)
    unit_description = models.CharField(max_length=50)

    def __str__(self):
        return self.unit_code
