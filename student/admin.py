from django.contrib import admin
from django.contrib.auth.models import Group
from .forms import UserAdminCreationForm, UserAdminChangeForm
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from .models import User, Unit, Faculty, Department, StudentProfile
from django.contrib.auth.models import Permission
from staff.models import StaffProfile
from missmark.models import StudentMissingMark, AcademicYears


class UserAdmin(DjangoUserAdmin):

    form = UserAdminChangeForm  # updateview
    add_form = UserAdminCreationForm  # create view

    list_display = ('email', 'fullname', 'admin', 'staff', 'student')
    list_filter = ('admin', 'staff', 'active')
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal info', {'fields': ('fullname',)}),
        ('Permissions', {'fields': ('admin', 'staff', 'active', 'student')}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'fullname', 'password1', 'password2')
        }),
    )

    search_fields = ['email']
    ordering = ('email',)
    filter_horizontal = ()


admin.site.register(User, UserAdmin)
admin.site.register(Unit)
admin.site.register(StudentMissingMark)
admin.site.register(AcademicYears)
admin.site.register(Faculty)
admin.site.register(StudentProfile)
admin.site.register(StaffProfile)
admin.site.register(Department)
admin.site.unregister(Group, )
# Register your models here.
