from django.urls import path
from student import views
urlpatterns = [
    path('', views.login_page, name='login'),
    path('register/', views.register, name='register'),
    path('index/', views.index, name='index'),
    path('Change Password/', views.change_password, name='change_password'),
    path('logout/', views.logout_view, name='logout'),
    path('current/', views.current, name='current'),
    path('pending/', views.pending, name='pending'),
    path('previous/', views.previous, name='previous'),
    path('ajax/load-departments/', views.load_departments, name='ajax_load_departments'),
    path('ajax/load-lecs/', views.load_lecs, name='ajax_load_lecs'),
    path('ajax/load-units/', views.load_units, name='ajax_load_units'),
    path('ajax/val_unit/', views.validate_missing_marks, name='validate_missing_marks'),
]
