from django import forms
from missmark.models import StudentMissingMark
from staff.models import StaffProfile
from student.models import Faculty, Department, Unit


class RegisterResitForm(forms.ModelForm):
    faculty = forms.ModelChoiceField(queryset=Faculty.objects.all().exclude(name__exact="Other"),
                                     widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = StudentMissingMark
        widgets = {
            'student_no': forms.HiddenInput,
            'seen_status': forms.HiddenInput,
            'mm_status': forms.HiddenInput,
            'department': forms.Select(attrs={'class': 'form-control'}),
            'taught_by': forms.Select(attrs={'class': 'form-control'}),
            'academic_year': forms.Select(attrs={'class': 'form-control'}),
            'unit': forms.Select(attrs={'class': 'form-control'}),
            'year': forms.Select(attrs={'class': 'form-control'}),
            'semester': forms.Select(attrs={'class': 'form-control'})
        }
        exclude = ['student_no', 'seen_status', 'mm_status']
        fields = ('faculty', 'unit', 'department', 'taught_by', 'year', 'semester', 'academic_year')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["department"].queryset = Department.objects.none()
        self.fields["taught_by"].queryset = StaffProfile.objects.none()
        self.fields['unit'].queryset = Unit.objects.none()

        if 'faculty' in self.data:
            try:
                faculty_id = int(self.data.get('faculty'))
                self.fields['department'].queryset = Department.objects.filter(faculty_id=faculty_id).order_by(
                    'name')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['department'].queryset = self.instance.faculty.department_set.order_by('name')

        if 'department' in self.data:
            try:
                department_id = int(self.data.get('department'))
                self.fields['taught_by'].queryset = StaffProfile.objects.filter(department_id=department_id).order_by(
                    'staff_no')
                self.fields['unit'].queryset = Unit.objects.filter(department_id=department_id).order_by('year_offered')
            except(ValueError, TypeError):
                pass
        elif self.instance.pk:
            self.fields['taught_by'].queryset = self.instance.department.taught_by_set.order_by('staff_no')
            self.fields['unit'].queryset = self.instance.department.unit_set.order_by('year_offered')
