# Generated by Django 2.0.5 on 2018-09-05 08:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0001_initial'),
        ('missmark', '0001_initial'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='studentmissingmark',
            unique_together={('student_no', 'unit')},
        ),
    ]
