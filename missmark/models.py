from django.db import models

# Create your models here.
from student.models import StudentProfile, Faculty, Department, Unit
from staff.models import StaffProfile


class AcademicYears(models.Model):
    academic_year = models.CharField(max_length=9, primary_key=True)

    def __str__(self):
        return self.academic_year


class StudentMissingMark(models.Model):
    student_no = models.ForeignKey(StudentProfile, on_delete=models.CASCADE)
    faculty = models.ForeignKey(Faculty, on_delete=models.CASCADE)
    academic_year = models.ForeignKey(AcademicYears, on_delete=models.CASCADE)
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    taught_by = models.ForeignKey(StaffProfile, on_delete=models.CASCADE)
    year_choices = ((1, 'I'), (2, 'II'), (3, 'III'), (4, 'IV'), (5, 'V'))
    year = models.PositiveSmallIntegerField(choices=year_choices)
    sem_choices = ((1, 'I'), (2, 'II'), (3, 'III'))
    semester = models.PositiveSmallIntegerField(choices=sem_choices)
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE)
    seen_status = models.BooleanField(default=False)
    mm_status = models.BooleanField(default=False)
    comment = models.TextField(blank=True)

    class Meta:
        unique_together = ('student_no', 'unit')

    @property
    def dep_name(self):
        return self.unit.department.name

    @property
    def faculty_name(self):
        return self.unit.department.faculty.name

    @property
    def unit_code(self):
        return self.unit.unit_code

    def __str__(self):
        template = '{0.student_no}  {0.unit}'
        return template.format(self)
