from django.apps import AppConfig


class MissmarkConfig(AppConfig):
    name = 'missmark'
