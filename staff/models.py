from student.models import User, Faculty, Department
from django.db import models


class StaffProfile(User):
    staff_no = models.CharField(max_length=15, primary_key=True)
    phone_no = models.CharField(max_length=10, unique=True)
    faculty = models.ForeignKey(Faculty, on_delete=models.CASCADE, default=1)
    department = models.ForeignKey(Department, on_delete=models.CASCADE, default=1)
    staff_roles = (
        (1, 'LECTURER'), (2, 'EXAMINATION OFFICER'), (3, 'CHAIR OF DEPARTMENT'), (4, 'DEAN'), (5, 'REGISTRAR'))
    role = models.PositiveSmallIntegerField(choices=staff_roles, default=1)
    gender_choices = ((2, 'MALE'), (3, 'FEMALE'))
    gender = models.PositiveSmallIntegerField(choices=gender_choices)

    def __str__(self):
        return self.fullname
