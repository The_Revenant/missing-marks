from django import forms

from student.models import Faculty, Department
from .models import StaffProfile
from missmark.models import StudentMissingMark


class RegisterStaffForm(forms.ModelForm):
    email = forms.EmailField(widget=forms.EmailInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter Employee Email'}))
    fullname = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter Employee Full Name'}))
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter New Password'}))
    password2 = forms.CharField(label='Password Confirmation', widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'placeholder': 'Confirm The Password'}))
    staff_no = forms.CharField(max_length=7, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter Employee Staff Number'}))
    phone_no = forms.CharField(max_length=10, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter Employee Phone Number'}))
    faculty = forms.ModelChoiceField(queryset=Faculty.objects.all(),
                                     widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = StaffProfile
        widgets = {'gender': forms.Select(attrs={'class': 'form-control'}),
                   'role': forms.Select(attrs={'class': 'form-control'}),
                   'department': forms.Select(attrs={'class': 'form-control'}),
                   }
        fields = ('email', 'fullname', 'staff_no', 'phone_no', 'gender', 'role', 'department', 'faculty')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["department"].queryset = Department.objects.none()

        if 'faculty' in self.data:
            try:
                faculty_id = int(self.data.get('faculty'))
                self.fields['department'].queryset = Department.objects.filter(faculty_id=faculty_id).order_by('name')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['department'].queryset = self.instance.faculty.department_set.order_by('name')

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords do not match")
        return password2

    def save(self, commit=True):
        user = super(RegisterStaffForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        user.set_password(self.cleaned_data["password1"])
        user.active = True
        user.staff = True
        if commit:
            user.save()
        return user


class LoginForm(forms.Form):
    email = forms.EmailField(label='Email', widget=forms.TextInput(attrs={'class': 'form-control',
                                                                          'placeholder': 'Enter Your Email Address'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control',
                                                                 'placeholder': 'Enter Your Password'}))


class SortResitForm(forms.ModelForm):
    class Meta:
        model = StudentMissingMark
        widgets = {'student_no': forms.Select(attrs={'class': 'form-control', 'readonly': 'readonly'}),
                   'unit': forms.Select(attrs={'class': 'form-control'}),
                   'comment': forms.Textarea(attrs={'class': 'form-control'})
                   }
        mm_status = forms.BooleanField(widget=forms.NullBooleanSelect(attrs={'class': 'form-control'}))
        seen_status = forms.BooleanField(widget=forms.NullBooleanSelect(attrs={'class': 'form-control'}))
        fields = ("student_no", "unit", "mm_status", "comment", "seen_status")

    def __init__(self, *args, **kwargs):
        super(SortResitForm, self).__init__(*args, **kwargs)
        self.fields['student_no'].disabled = True
        self.fields['unit'].disabled = True
