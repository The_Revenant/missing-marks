from django.urls import path
from . import views

urlpatterns = [
    path('login/', views.login_page, name='staff_login'),
    path('marks/', views.staff_view, name='welcome'),
    path('staff/<int:pk>', views.resit_update, name='resit_edit'),
    path('sorted/', views.sort_view, name='sorted'),
    path('partially/', views.partially, name='partially'),
    path('add/user/', views.registrar, name='registrar'),
    path('registrar/home/', views.registrar_home, name='registrar_home'),
    path('logout/', views.staff_logout_view, name='staff_logout'),
    path('lecturer/', views.lecturer_view, name='lecturer'),
    path('password/', views.staff_change_password, name='staff_password'),
    path('cod/', views.cod_view, name='cod'),
    path('dean/', views.dean_view, name='dean'),
    path('some/', views.some_view, name='some_view'),
    path('base/', views.base_view, name='base_view'),
    path('cod/pdf', views.GeneratePdf.as_view(), name='dpdf'),
    path('cod/pdf/1', views.PDF2018.as_view(), name='dpdf1'),
    path('cod/pdf/2', views.PDF2017.as_view(), name='dpdf2'),
    path('cod/pdf/3', views.PDF2016.as_view(), name='dpdf3'),
    path('cod/pdf/4', views.PDF2015.as_view(), name='dpdf4'),
    path('dean/pdf/', views.GenerateFacultyPdf.as_view(), name='fpdf'),
    path('dean/pdf/1', views.FacultyPdf1.as_view(), name='fpdf1'),
    path('dean/pdf/2', views.FacultyPdf2.as_view(), name='fpdf2'),
    path('dean/pdf/3', views.FacultyPdf3.as_view(), name='fpdf3'),
    path('dean/pdf/4', views.FacultyPdf4.as_view(), name='fpdf4')
]
