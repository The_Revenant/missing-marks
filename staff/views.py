from django.contrib import messages
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.db.models import Count
from django.template.loader import get_template
from mailer import send_mail
from missmark.models import StudentMissingMark, AcademicYears
from student.models import Department, Unit
from .forms import LoginForm, SortResitForm, RegisterStaffForm
from django.views.generic import View
from .utils import render_to_pdf


# Create your views here.
def login_page(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            request.session['email'] = email
            cd = form.cleaned_data
            user = authenticate(email=cd['email'],
                                password=cd['password'])
            if user is not None:
                login(request, user)
                if request.user.admin:
                    return redirect('admin')
                elif request.user.staffprofile.role is 1:
                    return redirect('lecturer')
                elif request.user.staffprofile.role is 2:
                    return redirect('welcome')
                elif request.user.staffprofile.role is 3:
                    return redirect('cod')
                elif request.user.staffprofile.role is 4:
                    return redirect('dean')
                elif request.user.staffprofile.role is 5:
                    return redirect('registrar_home')
            else:
                form = LoginForm()
                context = {'form': form}
                return render(request, 'accs/login.html', context)
        else:
            form = LoginForm()
            context = {'form': form}
            return render(request, 'accs/login.html', context)

    else:
        form = LoginForm()
    context = {'form': form}
    return render(request, 'accs/login.html', context)


def staff_view(request):
    if request.session.session_key is None:
        return redirect("login")
    if not request.user.staff:
        return redirect('index')
    else:
        resit_list = StudentMissingMark.objects.filter(mm_status=False, seen_status=False,
                                                       department=request.user.staffprofile.department)
        context = {'resit_list': resit_list}
        return render(request, 'staff/welcome.html', context)


def sort_view(request):
    if request.session.session_key is None:
        return redirect("staff_login")
    if not request.user.staff:
        return redirect('index')
    else:
        if request.method == 'POST':
            form = SortResitForm(request.POST)
            if form.is_valid():
                form.save()
        missing_marks_list = StudentMissingMark.objects.filter(mm_status=True,
                                                               department=request.user.staffprofile.department)
        context = {'missing_marks_list': missing_marks_list}
        return render(request, 'staff/sorted.html', context)


def partially(request):
    if request.session.session_key is None:
        return redirect("login")
    else:
        missing_marks_list = StudentMissingMark.objects.filter(seen_status=True, mm_status=False,
                                                               department=request.user.staffprofile.department)
        context = {'missing_marks_list': missing_marks_list}
        return render(request, 'staff/Partially.html', context)


def resit_update(request, pk):
    missing_marks = get_object_or_404(StudentMissingMark, pk=pk)
    missing_marks.student_no.email = missing_marks.student_no_id
    form = SortResitForm(request.POST or None, instance=missing_marks)

    if form.is_valid():
        form.save()
        return redirect('welcome')
    else:
        pr = missing_marks.student_no
        print(pr)
        form = SortResitForm(instance=missing_marks)
        cont = {'form': form}
        return render(request, 'staff/editresit.html', cont)


def registrar(request):
    if request.method == 'POST':
        staff_form = RegisterStaffForm(request.POST)
        if staff_form.is_valid():
            staff_form.save()
            messages.success(request, 'User Successfully added')
            return redirect('registrar_home')
        else:
            messages.error(request, 'Correct the error below')
    else:
        staff_form = RegisterStaffForm()
    cont = {'staff_form': staff_form}
    return render(request, 'registrar/staff.html', cont)


def registrar_home(request):
    return render(request, 'registrar/home.html')


def staff_logout_view(request):
    logout(request)
    return redirect('staff_login')


def lecturer_view(request):
    return render(request, 'lecturer/home.html')


def cod_view(request):
    field = 'unit'
    units_data = {}
    queryset = StudentMissingMark.objects.filter(department=request.user.staffprofile.department).values(
        field).annotate(
        count=Count(field))
    for item in queryset:
        result = Unit.objects.filter(unit_code=item['unit']).order_by('unit_code').get()
        units_data[result.unit_code] = item['count']

    print(units_data)
    send_mail("hello", "hey", "awambugu51@gmail,com", ["andrewwambugu93@gmail.com"], priority=None, fail_silently=False,
              auth_user="awambugu51@gmail.com", auth_password="Andy@1995")
    return render(request, 'cod/home.html', {'queryset': units_data})


def dean_view(request):
    global qu
    field = 'department'
    departments_data = {}
    academic_data = {}
    queryset = StudentMissingMark.objects.filter(faculty=request.user.staffprofile.faculty).values(field).annotate(
        count=Count(field))
    for item in queryset:
        result = Department.objects.filter(id=item['department']).order_by('department').get()
        departments_data[result.name] = item['count']
        print(departments_data)

    academic = AcademicYears.objects.all()
    fie = 'academic_year'
    for acad in academic:
        qu = StudentMissingMark.objects.filter(faculty=request.user.staffprofile.faculty,
                                               academic_year=acad).values(field).annotate(count=Count(fie))
        for q in qu:
            res = AcademicYears.objects.filter(academic_year=acad).order_by('academic_year').get()
            academic_data[res.academic_year] = q['count']
            print(academic_data)
    return render(request, 'dean/home.html', {'dep_list': departments_data, 'academic_year': academic_data})


def staff_change_password(request):
    if request.session.session_key is None:
        return redirect("login")
    else:
        if request.method == 'POST':
            form = PasswordChangeForm(request.user, request.POST)
            if form.is_valid():
                user = form.save()
                update_session_auth_hash(request, user)  # Important!
                messages.success(request, 'Your password was successfully updated!')
                if request.user.staffprofile.role is 1:
                    return redirect('lecturer')
                elif request.user.staffprofile.role is 2:
                    return redirect('welcome')
                elif request.user.staffprofile.role is 3:
                    return redirect('cod')
                elif request.user.staffprofile.role is 4:
                    return redirect('dean')
                elif request.user.staffprofile.role is 5:
                    return redirect('registrar_home')
            else:
                messages.error(request, 'Please correct the error below.')
        else:
            form = PasswordChangeForm(request.user)
        return render(request, 'accounts/change_password.html', {'form': form})


def some_view(request):
    resit_list = StudentMissingMark.objects.filter(mm_status=False, seen_status=False,
                                                   department=request.user.staffprofile.department)
    context = {'resit_list': resit_list}
    return render(request, 'mess/welcome.html', context)


def base_view(request):
    departments = Department.objects.order_by('faculty')
    context = {'departments': departments}
    return render(request, 'mess/base.html', context)


class GeneratePdf(View):
    def get(self, request, *args, **kwargs):
        field = 'unit'
        units_data = {}
        queryset = StudentMissingMark.objects.filter(department=request.user.staffprofile.department).values(
            field).annotate(count=Count(field))
        for item in queryset:
            result = Unit.objects.filter(unit_code=item['unit']).order_by('unit_code').get()
            units_data[result.unit_code] = item['count']
        context = {'queryset': units_data}
        pdf = render_to_pdf('pdf/department.html', context)
        return HttpResponse(pdf, content_type='application/pdf')


class GenerateFacultyPdf(View):
    def get(self, request):
        global qu
        field = 'department'
        departments_data = {}
        academic_data = {}
        queryset = StudentMissingMark.objects.filter(faculty=request.user.staffprofile.faculty).values(field).annotate(
            count=Count(field))
        for item in queryset:
            result = Department.objects.filter(id=item['department']).order_by('department').get()
            departments_data[result.name] = item['count']
            print(departments_data)

        academic = AcademicYears.objects.all()
        fie = 'academic_year'
        for acad in academic:
            qu = StudentMissingMark.objects.filter(faculty=request.user.staffprofile.faculty,
                                                   academic_year=acad).values(field).annotate(count=Count(fie))
            for q in qu:
                res = AcademicYears.objects.filter(academic_year=acad).order_by('academic_year').get()
                academic_data[res.academic_year] = q['count']
        context = {'dep_list': departments_data, 'academic_year': academic_data}
        pdf = render_to_pdf('pdf/faculty.html', context)
        return HttpResponse(pdf, content_type='application/pdf')


class PDF2018(View):
    def get(self, request, *args, **kwargs):
        field = 'unit'
        units_data = {}
        queryset = StudentMissingMark.objects.filter(department=request.user.staffprofile.department,
                                                     academic_year='2017/2018').values(
            field).annotate(
            count=Count(field))
        for item in queryset:
            result = Unit.objects.filter(unit_code=item['unit']).order_by('unit_code').get()
            units_data[result.unit_code] = item['count']
        context = {'queryset': units_data}
        pdf = render_to_pdf('pdf/department.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "2017/2018 MISSING MARKS REPORT"
            content = "inline; filename = '%s'" % filename
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not Found")


class PDF2017(View):
    def get(self, request, *args, **kwargs):
        field = 'unit'
        units_data = {}
        queryset = StudentMissingMark.objects.filter(department=request.user.staffprofile.department,
                                                     academic_year='2016/2017').values(
            field).annotate(
            count=Count(field))
        for item in queryset:
            result = Unit.objects.filter(unit_code=item['unit']).order_by('unit_code').get()
            units_data[result.unit_code] = item['count']
        context = {'queryset': units_data}
        pdf = render_to_pdf('pdf/department.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "2016/2017 MISSING MARKS REPORT"
            content = "inline; filename = '%s'" % filename
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not Found")


class PDF2016(View):
    def get(self, request, *args, **kwargs):
        field = 'unit'
        units_data = {}
        queryset = StudentMissingMark.objects.filter(department=request.user.staffprofile.department,
                                                     academic_year='2015/2016').values(
            field).annotate(
            count=Count(field))
        for item in queryset:
            result = Unit.objects.filter(unit_code=item['unit']).order_by('unit_code').get()
            units_data[result.unit_code] = item['count']
        context = {'queryset': units_data}
        pdf = render_to_pdf('pdf/department.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "2015/2016 MISSING MARKS REPORT"
            content = "inline; filename = '%s'" % filename
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not Found")


class PDF2015(View):
    def get(self, request, *args, **kwargs):
        field = 'unit'
        units_data = {}
        queryset = StudentMissingMark.objects.filter(department=request.user.staffprofile.department,
                                                     academic_year='2014/2015').values(
            field).annotate(
            count=Count(field))
        for item in queryset:
            result = Unit.objects.filter(unit_code=item['unit']).order_by('unit_code').get()
            units_data[result.unit_code] = item['count']
        context = {'queryset': units_data}
        pdf = render_to_pdf('pdf/department.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "2014/2015 MISSING MARKS REPORT"
            content = "inline; filename = '%s'" % filename
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not Found")


class FacultyPdf1(View):
    def get(self, request):
        global qu
        field = 'department'
        departments_data = {}
        queryset = StudentMissingMark.objects.filter(faculty=request.user.staffprofile.faculty,
                                                     academic_year='2017/2018').values(field).annotate(
            count=Count(field))
        for item in queryset:
            result = Department.objects.filter(id=item['department']).order_by('department').get()
            departments_data[result.name] = item['count']
            print(departments_data)

        context = {'dep_list': departments_data}
        pdf = render_to_pdf('pdf/faculty.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "2017/2018 FACULTY MISSING MARKS REPORT"
            content = "inline; filename = '%s'" % filename
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not Found")


class FacultyPdf2(View):
    def get(self, request):
        global qu
        field = 'department'
        departments_data = {}
        queryset = StudentMissingMark.objects.filter(faculty=request.user.staffprofile.faculty,
                                                     academic_year='2016/2017').values(field).annotate(
            count=Count(field))
        for item in queryset:
            result = Department.objects.filter(id=item['department']).order_by('department').get()
            departments_data[result.name] = item['count']
            print(departments_data)

        context = {'dep_list': departments_data}
        pdf = render_to_pdf('pdf/faculty.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "2016/2017 FACULTY MISSING MARKS REPORT"
            content = "inline; filename = '%s'" % filename
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not Found")


class FacultyPdf3(View):
    def get(self, request):
        global qu
        field = 'department'
        departments_data = {}
        queryset = StudentMissingMark.objects.filter(faculty=request.user.staffprofile.faculty,
                                                     academic_year='2015/2016').values(field).annotate(
            count=Count(field))
        for item in queryset:
            result = Department.objects.filter(id=item['department']).order_by('department').get()
            departments_data[result.name] = item['count']
            print(departments_data)

        context = {'dep_list': departments_data}
        pdf = render_to_pdf('pdf/faculty.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "2015/2016 FACULTY MISSING MARKS REPORT"
            content = "inline; filename = '%s'" % filename
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not Found")


class FacultyPdf4(View):
    def get(self, request):
        global qu
        field = 'department'
        departments_data = {}
        queryset = StudentMissingMark.objects.filter(faculty=request.user.staffprofile.faculty,
                                                     academic_year='2014/2015').values(field).annotate(
            count=Count(field))
        for item in queryset:
            result = Department.objects.filter(id=item['department']).order_by('department').get()
            departments_data[result.name] = item['count']
            print(departments_data)

        context = {'dep_list': departments_data}
        pdf = render_to_pdf('pdf/faculty.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "2014/2015 FACULTY MISSING MARKS REPORT"
            content = "inline; filename = '%s'" % filename
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not Found")
