from distutils.core import setup

setup(
    name='MissingMarks',
    version='1.0',
    packages=['staff', 'staff.migrations', 'student', 'student.migrations', 'missmark', 'missmark.migrations',
              'MissingMarks'],
    url='',
    license='',
    author='andy',
    author_email='awambugu51@gmail.com',
    description='EGERTON UNIVERSITY MISSING MARKS MANAGEMENT SYSTEM', requires=['django', 'xhtml2pdf',
                                                                                'django-notifications-hq',
                                                                                'django-mailer']
)
